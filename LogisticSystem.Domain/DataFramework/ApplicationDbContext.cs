﻿using LogisticSystem.Domain.Entities;
using System.Data.Entity;

namespace LogisticSystem.Domain.DataFramework
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("ApplicationDbConnection")
        {
        }

        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Category>().HasOptional(n => n.Parent).WithMany(p => p.Children);

        }
    }
}

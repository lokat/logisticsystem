namespace LogisticSystem.Domain.Migrations
{
    using DataFramework;
    using Entities;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            context.Categories.AddOrUpdate(
              c => c.Id,
              new Category { Id = 1, Name = "Technics", ParentId = null },
              new Category { Id = 2, Name = "Transport", ParentId = 1 },
              new Category { Id = 3, Name = "Passenger", ParentId = 2 },
              new Category { Id = 4, Name = "Cargo", ParentId = 2 },
              new Category { Id = 5, Name = "Energy", ParentId = null },
              new Category { Id = 6, Name = "Gas", ParentId = 5 },
              new Category { Id = 7, Name = "Electricity", ParentId = 5 }
            );

        }
    }
}

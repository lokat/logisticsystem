﻿$(function () {
    var tree = $('#tree');
    tree.jstree({
        'core': {
            'data': {
                'url': function (node) {
                    return tree.data('url') + node.id;
                },
                'data': function (node) {
                    return {
                        'id': node.Id
                    };
                }
            }
        }
    });
});
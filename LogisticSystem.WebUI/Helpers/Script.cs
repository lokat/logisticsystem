﻿using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Optimization;

namespace LogisticSystem.WebUI.Helpers
{
    public enum ScriptPosition
    {
        Top,
        Bottom
    };
    public static class Script
    {
        private const string ScriptBundlePrefix = "ScriptBundle";

        public static MvcHtmlString RenderScriptBundle(this HtmlHelper htmlHelper, ScriptPosition scriptPosition)
        {
            var result = new StringBuilder();
            var key = Script.ScriptBundlePrefix + scriptPosition;
            var bundleList = htmlHelper.ViewContext.HttpContext.Items[key] as List<string>;

            if (bundleList != null)
            {
                foreach (var bundle in bundleList)
                {
                    result.AppendLine(Scripts.Render(bundle).ToString());
                }
            }
            return MvcHtmlString.Create(result.ToString());
        }

        public static void RegisterScriptBundle(this HtmlHelper htmlHelper, string bundle, ScriptPosition scriptPosition)
        {
            var key = Script.ScriptBundlePrefix + scriptPosition;
            var bundleList = htmlHelper.ViewContext.HttpContext.Items[key] as List<string>;

            if (bundleList != null)
            {
                if (!bundleList.Contains(bundle))
                {
                    bundleList.Add(bundle);
                }
            }
            else
            {
                bundleList = new List<string> { bundle };
                htmlHelper.ViewContext.HttpContext.Items.Add(key, bundleList);
            }
        }
    }
}
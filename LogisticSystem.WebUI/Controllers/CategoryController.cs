﻿using LogisticSystem.Domain.Entities;
using LogisticSystem.Repositories.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Linq;
using LogisticSystem.WebUI.Filters;

namespace LogisticSystem.WebUI.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        [HttpGet, OutputCache(Duration = 3600), AjaxOnly]
        public JsonResult GetChildren(int? id)
        {
            var categories = _categoryRepository
                .Find(c => c.ParentId == id)
                .Select(c => new {id = c.Id, text = c.Name, children = c.Children.Any()});
            return new JsonResult() {Data = categories, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }
    }
}
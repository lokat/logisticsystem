﻿using LogisticSystem.Domain.Entities;

namespace LogisticSystem.Repositories.Abstract
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}

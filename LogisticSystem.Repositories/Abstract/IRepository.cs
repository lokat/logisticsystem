﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace LogisticSystem.Repositories.Abstract
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> filter);
    }
}

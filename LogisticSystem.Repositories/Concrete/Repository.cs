﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Linq;

using LogisticSystem.Domain.DataFramework;
using LogisticSystem.Repositories.Abstract;

namespace LogisticSystem.Repositories.Concrete
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly ApplicationDbContext _dbContext = new ApplicationDbContext();

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> filter)
        {
            return _dbContext.Set<TEntity>().Where(filter).ToList();
        }
    }
}

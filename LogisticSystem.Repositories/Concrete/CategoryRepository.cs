﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LogisticSystem.Domain.Entities;
using LogisticSystem.Repositories.Abstract;

namespace LogisticSystem.Repositories.Concrete
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
    }
}
